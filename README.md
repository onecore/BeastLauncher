MuOnline Launcher Zen 
============

Invictuz Online Game Launcher
![alt tag](http://a.fsdn.com/con/app/proj/muonlinelauncher/screenshots/CGO9L0k.png)


Informations
============
Author: Mark Anthony Pequeras
Developers: CoreSEC Software Development
Language: Python 2.7


Version
============
1.0
- Buttons with Webview
- Settings

2.0 
- Buttons with Webview
- Settings
- Updater/Patcher Added
- .Zip Unzipper

3.0 
- Buttons with Webview
- Settings
- Updater/Patcher Added
- .Zip Unzipper
- Seperate Patcher (Runs if New Versions Detected)

4.0 
- Buttons with Webview
- Settings
- Updater/Patcher Added
- .Zip Unzipper
- Seperate Patcher (Runs if New Versions Detected)
- Updater can only Update on Single Version (e.g 5.0,5.1,5.2,5.3)

5.0 
- Fixes and Performance Improvements

6.0 
- Splash Added
- No Server name Option
- Message Tab
- Resolution Settings Fixes
- Errors are now Prompt+Messages
- Updater.exe Fix
- Files are lesser
- Anti-Hack Option (Requires: BeastAH.py)


Downloads
===========
https://forum.ragezone.com/f197/muonline-launcher-zen-updated-version-1067139/